# README

Some personal usefull docker services used on PI4.

## pi-hole stack

Resonable stack to run pi-hole with some DoH (DNS over HTTPS) to protect 'some' privacy while blocking 'some' adds.

Based on [cloudflared](https://github.com/visibilityspots/dockerfile-cloudflared) and [pihole](https://github.com/pi-hole/docker-pi-hole)

Requres .env file to be present. Example template in repository.

## run

```bash
sudo docker compose up --detach
```

## interface

pi-hole admin interface is exposed under 8020 port: `http://${SERVER_IP}:8020/admin`

## volumes

pi-hole configuration and databases are bind mounts, hence they are avaiable in `~/docker/pihole/` folder.

## dns over https (DoH)

[Quad9](https://www.quad9.net/news/blog/doh-with-quad9-dns-servers/) DNS with DNSSEQ upstream servers are used.

## router setup

For [OpenWrt](https://openwrt.org/) or any non stock router firmware advertise iP-hole's IP address via dnsmasq in the router.
Benefits

- Per-host tracking on Pi-hole
- The ability to resolve hostnames on the LAN
- Ad blocking/network monitoring provided by Pi-hole

On the router, use a custom dnsmasq config entry to advertise the IP of the Pi-hole box (example IP).
DHCP-Options`6,10.0.0.1`

Additionally setup **only one** Static custom DNS pointing to pi-hole ${SERVER_IP}
